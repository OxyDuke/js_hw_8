//1 завдання
const changeBackgroundColor = () => {
    const paragraphs = document.getElementsByTagName("p");

    for (const paragraph of paragraphs) {
        paragraph.style.backgroundColor = "#ff0000";
    }
}
changeBackgroundColor();

// 2 завдання
const optionsListElement = document.getElementById("optionsList");
console.log("Елемент з id 'optionsList':", optionsListElement);

if (optionsListElement) {
    const parentElement = optionsListElement.parentElement;
    console.log("Батьківський елемент:", parentElement);

    const childNodes = optionsListElement.childNodes;
    console.log("Дочірні ноди:");

    childNodes.forEach(node => {
        if (node.nodeType === Node.ELEMENT_NODE) {
            console.log("Назва:", node.nodeName, "Тип:", node.nodeType);
        }
    });
}
 
//3 завдання
const testParagraphElement = document.querySelector('.testParagraph');
if (testParagraphElement) {
    testParagraphElement.textContent = 'This is a paragraph';
}

//4, 5 завдання
const mainHeaderElement = document.querySelector('.main-header');
if (mainHeaderElement) {
    const nestedElements = mainHeaderElement.querySelectorAll('*');
    nestedElements.forEach(element => {
        element.classList.add('nav-item');
    });
    console.log("Вкладені елементи в .main-header:", nestedElements);
}
//6 завдання
const sectionTitleElements = document.querySelectorAll('.section-title');
sectionTitleElements.forEach(element => {
    element.classList.remove('section-title');
});
